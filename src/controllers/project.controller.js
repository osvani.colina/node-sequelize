import Project from '../models/Project';


export async function getProjects(req, res) {
    try{
        const projects = await Project.findAll();   
        res.json({
            data: projects
        })
    }catch(e){
        console.log(e);
        res.status(500).json({
            message: 'Something goes wrong!',
            data: {}
        });
    }   
};

export async function createProject(req, res) {
    const {name, priority, description, deliverydate} = req.body;
    
    try{
        let newProject = await Project.create({
            name,
            priority,
            description,
            deliverydate
        }, {
            fields: ['name', 'priority', 'description', 'deliverydate']
        });

        if(newProject){
            res.json({
                message: 'Project created sucessfully!',
                data: newProject
            });
        }
    }catch(e){
        console.log(e);
        res.status(500).json({
            message: 'Something goes wrong!',
            data: {}
        });
    }
};

export async function getOneProject(req, res) {
    const {id} = req.params;
    let project = await Project.findOne({
        where: {
            id
        }
    });
    res.json({
        data: project
    });
}

export async function deleteProject(req, res) {
    const {id} = req.params;
    let deleteRowCount = await Project.destroy({
        where: {
            id
        }
    });
    res.json({
        message: 'Project deleted successfully',
        data: deleteRowCount
    });
}

export async function updateProject(req, res){
    const {id} = req.params;
    const { name, priority, description, deliverydate } = req.body;
    const project = await Project.update({
        name,
        priority,
        description, 
        deliverydate
    }, {
        returning: true,
        where: {
        id
    }});
    res.json({
        message: 'Project update sucessfully',
        data: project
    });
}