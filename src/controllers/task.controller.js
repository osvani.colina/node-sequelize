import Task from '../models/Tasks';

export async function createTask(req, res) {
    const {name, done, projectid} = req.body;
    let newTask = await Task.create({
        name, 
        done, 
        projectid
    }, {
        fields: ['name', 'done', 'projectid']
    });

    res.json({
        message: 'New Task created',
        data: newTask
    });
}

export async function getTasks(req, res) {
    const tasks = await Task.findAll({
        attributes: ['id', 'projectid', 'name', 'done'], 
        order: [
            ['id', 'DESC']
        ]
    });

    res.json({
        data: tasks
    })
}

export async function getOneTask(req, res) {
    const { id } = req.params;

    const task = await Task.findOne({
        where: {
            id
        }
    });
    res.json(task);
    
}

export async function deleteTask(req, res) {
    const { id } = req.params;
    await Task.destroy({
        where: {
            id
        }
    });
    res.json({
        message: 'Task deleted'
    });
}

export async function updateTask(req, res) {
    const { id } = req.params;
    const { projectid, name, done } = req.body;
    const task = await Task.update({
        projectid,
        name,
        done
    }, {
        returning: true,
        where: {
            id       
    }});
    res.json({
        message:'Task updated',
        data: task
    });
}

export async function getTasksByProject(req, res) {
    const {projectid} = req.params;
    const tasks = await Task.findAll({
        where: {
            projectid
        }
    })
    res.json(tasks);
}